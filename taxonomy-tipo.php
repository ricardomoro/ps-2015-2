<?php get_header(); ?>

<?php breadcrumb(); ?>

<section class="container" id="content">
    <div class="row">
        <div class="col-xs-12 col-md-8">
            <h2 class="title">Editais do tipo &quot;<?php echo single_term_title(); ?>&quot;</h2>
            <?php echo get_template_part('partials/loop', 'editais'); ?>
        </div>
        <div class="col-xs-12 col-md-4">
            <?php if (!dynamic_sidebar('banner')) : endif; ?>
            <?php echo get_template_part('partials/edital', 'tipos'); ?>
        </div>
    </div>
</section>

<?php get_footer(); ?>
