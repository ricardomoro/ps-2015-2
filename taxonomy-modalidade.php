<?php get_header(); ?>

<?php breadcrumb(); ?>

<section class="container" id="content">
    <div class="row">
        <div class="col-xs-12 col-md-8">
            <h2>Cursos na modalidade de ensino &quot;<?php single_term_title(); ?>&quot;</h2>
            <!-- Nav tabs -->
            <ul class="nav nav-pills nav-campus" role="tablist">
                <li class="active"><a href="#tabTodos" role="tab" data-toggle="tab">Todos</a></li>
                <?php $terms = get_terms('campus'); ?>
                <?php foreach ($terms as $key => $campus) : ?>
                    <!-- <li <?php echo ($key == 0) ? 'class="active"' : ''; ?>><a href="#tab<?php echo $campus->term_id; ?>" role="tab" data-toggle="tab"><?php echo $campus->name; ?></a></li> -->
                    <li><a href="#tab<?php echo $campus->term_id; ?>" role="tab" data-toggle="tab"><?php echo $campus->name; ?></a></li>
                <?php endforeach; ?>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane fade in active" id="tabTodos">
                    <table class="table table-striped table-cursos">
                        <thead>
                            <tr>
                                <th>Curso</th>
                                <th>C&acirc;mpus</th>
                                <th>Turnos</th>
                                <th>Vagas *</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                global $wp_query;
                                $args = array(
                                    'post_type' => 'curso',
                                    'orderby' => 'title',
                                    'order' => 'ASC',
                                );
                                $args = array_merge($wp_query->query_vars, $args);
                                query_posts($args);
                            ?>
                            <?php while ( have_posts() ) : the_post(); ?>
                                <tr>
                                    <td><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></td>
                                    <td>
                                        <?php foreach (get_the_terms(get_the_ID(), 'campus') as $campus) : ?>
                                            <p><?php echo $campus->name; ?></p>
                                        <?php endforeach; ?>
                                    </td>
                                    <td>
                                        <?php foreach (get_the_terms(get_the_ID(), 'turno') as $turno) : ?>
                                            <p><?php echo $turno->name; ?></p>
                                        <?php endforeach; ?>
                                    </td>
                                    <td>
                                        <p><?php echo get_post_meta(get_the_ID(), 'vagas', true); ?></p>
                                    </td>
                                </tr>
                            <?php endwhile;?>
                            <?php wp_reset_query(); ?>
                        </tbody>
                    </table>
                </div>
                <?php foreach ($terms as $key => $campus) : ?>
                <!-- <div class="tab-pane fade<?php echo ($key == 0) ? ' in active' : ''; ?>" id="tab<?php echo $campus->term_id; ?>"> -->
                <div class="tab-pane fade" id="tab<?php echo $campus->term_id; ?>">
                    <table class="table table-striped table-cursos">
                        <thead>
                            <tr>
                                <th>Curso</th>
                                <th>C&acirc;mpus</th>
                                <th>Turnos</th>
                                <th>Vagas *</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                global $wp_query;
                                $args = array(
                                    'post_type' => 'curso',
                                    'orderby' => 'title',
                                    'order' => 'ASC',
                                    'campus' => $campus->slug,
                                );
                                $args = array_merge($wp_query->query_vars, $args);
                                query_posts($args);
                            ?>
                            <?php while ( have_posts() ) : the_post(); ?>
                                <tr>
                                    <td><a href="<?php echo get_permalink() ?>"><?php the_title(); ?></a></td>
                                    <td>
                                        <?php foreach (get_the_terms(get_the_ID(), 'campus') as $campus) : ?>
                                            <!-- <p><a href="<?php echo get_term_link($campus); ?>"><?php echo $campus->name; ?></a></p> -->
                                            <p><?php echo $campus->name; ?></p>
                                        <?php endforeach; ?>
                                    </td>
                                    <td>
                                        <?php foreach (get_the_terms(get_the_ID(), 'turno') as $turno) : ?>
                                            <p><?php echo $turno->name; ?></p>
                                        <?php endforeach; ?>
                                    </td>
                                    <td>
                                        <p><?php echo get_post_meta(get_the_ID(), 'vagas', true); ?></p>
                                    </td>
                                </tr>
                            <?php endwhile;?>
                            <?php wp_reset_query(); ?>
                        </tbody>
                    </table>
                </div>
                <?php endforeach; ?>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="alert alert-warning" role="alert">
                        <p><strong>*</strong> Para ver a forma de distribui&ccedil;&atilde;o das vagas, leia os <a href="<?php echo get_post_type_archive_link( 'edital' ); ?>">editais</a>.</p>
                    </div>
                </div>
            </div>
            <a href="<?php echo get_post_type_archive_link( 'curso' ); ?>" class="btn btn-default"><span class="glyphicon glyphicon-arrow-left"></span>&nbsp;Voltar para a lista de cursos</a>
        </div>
        <div class="col-xs-12 col-md-4">
            <aside>
                <?php if (!dynamic_sidebar('banner')) : endif; ?>
            </aside>
        </div>
    </div>
</section>

<?php get_footer(); ?>
