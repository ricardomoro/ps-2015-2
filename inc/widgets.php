<?php
function widgets_init() {
//widget "Banner"
	register_sidebar( array(
		'name' => 'Home Banner',
		'id' => 'banner',
		'description' => 'Banner na página inicial.',
		'before_widget' => ' <!--widget--><div id="%1$s" class="widget banner %2$s">',
		'after_widget'  => '<div class="clearfix"></div> </div><!--//widget-->',
		'before_title'  => '<h3 class="sr-only">',
		'after_title'   => '</h3> ',
		) );
	} 

//finaliza widget 
add_action( 'widgets_init', 'widgets_init' );
