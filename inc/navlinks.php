<?php
add_filter('next_posts_link_attributes', 'posts_link_attributes_next');
add_filter('previous_posts_link_attributes', 'posts_link_attributes_prev');

function posts_link_attributes_next() {
    return 'class="btn btn-primary"';
}
function posts_link_attributes_prev() {
    return 'class="btn btn-default"';
}
