<?php get_header(); ?>

<?php breadcrumb(); ?>

<section class="container" id="content">
    <div class="row">
        <div class="col-xs-12 col-md-8">
            <h2 class="title">Lista de Cursos<?php if (is_search() && get_search_query()) : ?><small>&nbsp;(Resultados da busca por &ldquo;<?php echo get_search_query(); ?>&rdquo;)</small><?php endif; ?></h2>
            <div class="row">
                <div class="col-xs-12 col-md-6 col-md-offset-6">
                    <form class="inline-form" method="get" action="." role="form">
                        <div class="input-group">
                            <label class="sr-only" for="s">Termo da busca</label>
                            <input class="form-control" type="text" value="<?php echo (get_search_query() ? get_search_query() : ''); ?>" name="s" id="s" placeholder="Digite sua busca..."/>
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-primary">Buscar</button>
                            </span>
                        </div>
                    </form>
                    <br/>
                </div>
            </div>
            <!-- Nav tabs -->
            <ul class="nav nav-pills nav-campus" role="tablist">
                <li class="active"><a href="#tabTodos" role="tab" data-toggle="tab">Todos</a></li>
                <?php $terms = get_terms('campus'); ?>
                <?php foreach ($terms as $key => $campus) : ?>
                    <li><a href="#tab<?php echo $campus->term_id; ?>" role="tab" data-toggle="tab"><?php echo $campus->name; ?></a></li>
                <?php endforeach; ?>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane fade in active" id="tabTodos">
                    <table class="table table-striped table-cursos">
                        <thead>
                            <tr>
                                <th>Curso</th>
                                <th>C&acirc;mpus</th>
                                <th>Modalidade</th>
                                <th>Turnos</th>
                                <th>Vagas *</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                global $wp_query;
                                $args = array(
                                    'orderby' => 'title',
                                    'order' => 'ASC',
                                );
                                $args = array_merge($wp_query->query_vars, $args);
                                query_posts($args);
                            ?>
                            <?php while ( have_posts() ) : the_post(); ?>
                                <tr>
                                    <td><a href="<?php echo get_permalink() ?>"><?php the_title(); ?></a></td>
                                    <td>
                                        <?php foreach (get_the_terms(get_the_ID(), 'campus') as $campus) : ?>
                                            <p><?php echo $campus->name; ?></p>
                                        <?php endforeach; ?>
                                    </td>
                                    <td>
                                        <?php foreach (get_the_terms(get_the_ID(), 'modalidade') as $modalidade) : ?>
                                            <p><a href="<?php echo get_term_link($modalidade); ?>"><?php echo $modalidade->name; ?></a></p>
                                        <?php endforeach; ?>
                                    </td>
                                    <td>
                                        <?php foreach (get_the_terms(get_the_ID(), 'turno') as $turno) : ?>
                                            <p><?php echo $turno->name; ?></p>
                                        <?php endforeach; ?>
                                    </td>
                                    <td>
                                        <p><?php echo get_post_meta(get_the_ID(), 'vagas', true); ?></p>
                                    </td>
                                </tr>
                            <?php endwhile;?>
                            <?php wp_reset_query(); ?>
                        </tbody>
                    </table>
                </div>
                <?php foreach ($terms as $key => $campus) : ?>
                <div class="tab-pane fade" id="tab<?php echo $campus->term_id; ?>">
                    <table class="table table-striped table-cursos">
                        <thead>
                            <tr>
                                <th>Curso</th>
                                <th>C&acirc;mpus</th>
                                <th>Modalidade</th>
                                <th>Turnos</th>
                                <th>Vagas *</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                global $wp_query;
                                $args = array(
                                    'post_type' => 'curso',
                                    'orderby' => 'title',
                                    'order' => 'ASC',
                                    'campus' => $campus->slug,
                                );
                                $args = array_merge($wp_query->query_vars, $args);
                                query_posts($args);
                            ?>
                            <?php while ( have_posts() ) : the_post(); ?>
                                <tr>
                                    <td><a href="<?php echo get_permalink() ?>"><?php the_title(); ?></a></td>
                                    <td>
                                        <?php foreach (get_the_terms(get_the_ID(), 'campus') as $campus) : ?>
                                            <p><?php echo $campus->name; ?></p>
                                        <?php endforeach; ?>
                                    </td>
                                    <td>
                                        <?php foreach (get_the_terms(get_the_ID(), 'modalidade') as $modalidade) : ?>
                                            <p><a href="<?php echo get_term_link($modalidade); ?>"><?php echo $modalidade->name; ?></a></p>
                                        <?php endforeach; ?>
                                    </td>
                                    <td>
                                        <?php foreach (get_the_terms(get_the_ID(), 'turno') as $turno) : ?>
                                            <p><?php echo $turno->name; ?></p>
                                        <?php endforeach; ?>
                                    </td>
                                    <td>
                                        <p><?php echo get_post_meta(get_the_ID(), 'vagas', true); ?></p>
                                    </td>
                                </tr>
                            <?php endwhile;?>
                            <?php wp_reset_query(); ?>
                        </tbody>
                    </table>
                </div>
                <?php endforeach; ?>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="alert alert-warning" role="alert">
                        <p><strong>*</strong> Para ver a forma de distribui&ccedil;&atilde;o das vagas, leia os <a href="<?php echo get_post_type_archive_link( 'edital' ); ?>">editais</a>.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-4">
            <div class="row">
                <div class="col-xs-12">
                    <?php if (!dynamic_sidebar('banner')) : endif; ?>
                    <br/>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>
