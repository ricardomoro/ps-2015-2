<?php get_header(); ?>

<?php breadcrumb(); ?>

<?php
    $cur_cat_id = get_cat_id( single_cat_title('',false) ); 
    $category = get_category($cur_cat_id);
    $category = $category->slug;
?>

<section class="container" id="content">
    <div class="row" id="home-banners">
        <div class="col-xs-12 col-md-4">
            <?php if (!dynamic_sidebar('banner')) : endif; ?>
        </div>
        <div class="col-xs-12 col-md-8">
            <?php echo do_shortcode('[image-carousel twbs="3"]'); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
        <h2 class="title"><?php single_cat_title( 'Categoria&nbsp;', true ); ?></h2>
        </div>
    </div>
    <div class="row">
        <?php echo do_shortcode('[ajax_load_more post_type="post" posts_per_page="4" scroll="false" transition="fade" category="'.$category.'" button_label="Carregar mais"]'); ?>
    </div>
</section>

<?php get_footer(); ?>
