<?php
//Registra os menus
register_nav_menus(
    array(
        'main' => 'Menu Principal',
        'footer' => 'Menu do Rodapé (Sitemap)',
    )
);

// Scripts & Styles específicos
require_once('inc/enqueue.php');

// Widgets
require_once('inc/widgets.php');

// Tamanho do excerpt
require_once('inc/excerpt.php');

// Navlinks personalizados
require_once('inc/navlinks.php');

// Adicionar PrettyPhoto automaticamente.
require_once('inc/prettyphoto_rel.php');
