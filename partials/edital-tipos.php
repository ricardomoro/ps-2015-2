<div class="row">
    <div class="col-xs-12">
        <h3>Tipos de Edital</h3>
        <div class="list-group">
        <?php $tipos_terms = get_terms( 'tipo' ); ?>
        <?php foreach ($tipos_terms as $key => $tipo) : ?>
            <a href="<?php echo get_term_link($tipo); ?>" class="list-group-item">
                <?php echo $tipo->name; ?>
            </a>
        <?php endforeach; ?>
        </div>
    </div>
</div>
