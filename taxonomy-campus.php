<?php
    global $wp_query;
    $args = array(
        'post_type' => 'curso',
        'orderby' => 'title',
        'order' => 'ASC',
    );
    $args = array_merge($wp_query->query_vars, $args);
    query_posts($args);
?>

<?php get_header(); ?>

<?php breadcrumb(); ?>

<section class="container" id="content">
    <div class="row">
        <div class="col-xs-12 col-md-8">
            <h2> Cursos no C&acirc;mpus <?php single_tag_title(); ?></h2>
            <article>
                <table class="table table-bordered table-striped table-cursos">
                    <thead>
                        <tr>
                            <th>Curso</th>
                            <th>Modalidade</th>
                            <th>Turnos</th>
                            <th>Vagas *</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php while ( have_posts() ) : the_post(); ?>
                            <tr>
                                <td><a href="<?php echo get_permalink() ?>"><?php the_title(); ?></a></td>
                                <td>
                                    <?php foreach (get_the_terms(get_the_ID(), 'modalidade') as $modalidade) : ?>
                                        <p><a href="<?php echo get_term_link($modalidade); ?>"><?php echo $modalidade->name; ?></a></p>
                                    <?php endforeach; ?>
                                </td>
                                <td>
                                    <?php foreach (get_the_terms(get_the_ID(), 'turno') as $turno) : ?>
                                        <p><?php echo $turno->name; ?></p>
                                    <?php endforeach; ?>
                                </td>
                                <td>
                                    <p><?php echo get_post_meta(get_the_ID(), 'vagas', true); ?></p>
                                </td>
                            </tr>
                        <?php endwhile;?>
                    </tbody>
                </table>
            </article>
            <div class="alert alert-warning" role="alert">
                <p><strong>*</strong> Para ver a forma de distribui&ccedil;&atilde;o das vagas, leia os <a href="<?php echo get_post_type_archive_link( 'edital' ); ?>">editais</a>.</p>
            </div>
            <a href="<?php echo get_post_type_archive_link( 'curso' ); ?>" class="btn btn-default"><span class="glyphicon glyphicon-arrow-left"></span>&nbsp;Voltar para a lista de cursos</a>
        </div>
        <div class="col-xs-12 col-md-4">
            <aside>
                <?php if (!dynamic_sidebar('banner')) : endif; ?>
            </aside>
        </div>
    </div>
</section>

<?php get_footer(); ?>
