<?php get_header(); ?>

<?php breadcrumb(); ?>

<?php
    global $wp_query;
    $args = array(
        'orderby' => 'menu_order title',
        'order' => 'DESC',
        'post_parent' => 0,
    );
    $args = array_merge($wp_query->query_vars, $args);
    query_posts($args);
?>

<section class="container" id="content">
    <div class="row">
        <div class="col-xs-12 col-md-8">
            <h2 class="title">Editais</h2>
            <?php echo get_template_part('partials/loop', 'editais'); ?>
        </div>
        <div class="col-xs-12 col-md-4">
            <div class="row">
                <div class="col-xs-12">
                    <?php if (!dynamic_sidebar('banner')) : endif; ?>
                    <br/>
                </div>
            </div>
            <?php echo get_template_part('partials/edital', 'tipos'); ?>
        </div>
    </div>
</section>

<?php get_footer(); ?>
