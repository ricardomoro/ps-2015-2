<?php
    global $wp_query;
    $args = array(
        'order' => 'ASC',
    );
    $args = array_merge($wp_query->query_vars, $args);
    query_posts($args);
?>

<?php get_header(); ?>

<?php breadcrumb(); ?>

<section class="container" id="content">
    <div class="row">
        <div class="col-xs-12 col-md-8">
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="title">Perguntas Frequentes<?php if (is_search() && get_search_query()) : ?><small>&nbsp;(Resultados da busca por &ldquo;<?php echo get_search_query(); ?>&rdquo;)</small><?php endif; ?></h2>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-6 col-md-offset-6">
                    <form class="inline-form" method="get" action="." role="form">
                        <div class="input-group">
                            <label class="sr-only" for="s">Termo da busca</label>
                            <input class="form-control" type="text" value="<?php echo (get_search_query() ? get_search_query() : ''); ?>" name="s" id="s" placeholder="Digite sua busca..."/>
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-primary">Buscar</button>
                            </span>
                        </div>
                    </form>
                    <br/>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel-group" id="accordion">
                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">
                                    <span class="caret"></span>&nbsp;<a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php the_ID(); ?>"><?php the_title(); ?></a>
                                </h3>
                            </div>
                            <div id="collapse<?php the_ID(); ?>" class="panel-collapse collapse<?php if (is_search() && get_search_query()) : echo ' in'; endif; ?>">
                                <div class="panel-body">
                                    <?php the_content(); ?>
                                </div>
                            </div>
                        </div>
                    <?php endwhile; endif; ?>
                    <?php wp_reset_query(); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-4">
            <?php if (!dynamic_sidebar('banner')) : endif; ?>
        </div>
    </div>
</section>

<?php get_footer(); ?>
