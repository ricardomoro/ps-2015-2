<?php get_header(); ?>

<section class="container" id="content">
    <div class="row" id="home-banners">
        <div class="col-xs-12 col-md-4">
            <?php if (!dynamic_sidebar('banner')) : endif; ?>
        </div>
        <div class="col-xs-12 col-md-8">
            <?php echo do_shortcode('[image-carousel twbs="3"]'); ?>
        </div>
    </div>

    <?php
        global $wp_query;
        $args = array(
            'tax_query' => array(
                array(
                    'taxonomy' => 'category',
                    'field'    => 'slug',
                    'terms'    => 'avisos',
                ),
            ),
            'posts_per_page' => 7,
        );
        $args = array_merge($wp_query->query_vars, $args);
        query_posts($args);
    ?>

    <?php if (have_posts()) : ?>
        <div class="row">
            <?php $first_post = true; ?>
            <?php while (have_posts()) : the_post(); ?>
                <?php if ($first_post) : ?>
                    <div class="col-xs-12 col-md-6">
                <?php else : ?>
                    <div class="col-xs-12 col-md-3">
                <?php endif; ?>
                        <article class="home-aviso">
                            <div class="row">
                                <div class="col-xs-4 <?php echo ($first_post ? 'col-sm-2' : 'col-sm-3'); ?>">
                                    <div class="aviso-data<?php echo ($first_post ? ' destaque' : ''); ?>">
                                        <?php the_time('M') ?>
                                        <br/>
                                        <span class="dia"><?php the_time('d') ?></span>
                                    </div>
                                </div>
                                <div class="col-xs-8 <?php echo ($first_post ? 'col-sm-10' : 'col-sm-9'); ?>">
                                    <div class="aviso-titulo">
                                        <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                                    </div>
                                    <?php if ($first_post) : ?>
                                        <p><?php the_excerpt(); ?></p>
                                    <?php endif; ?>

                                    <?php $cats = get_the_category(); ?>
                                    <?php if (!empty($cats)) : ?>
                                        <ul class="aviso-categorias">
                                            <?php foreach ($cats as $key => $cat) : ?>
                                                <li><a class="label label-<?php echo $cat->slug; ?>" href="<?php echo get_category_link( $cat->term_id ); ?>"><span class="sr-only">Categoria </span><?php echo $cat->name; ?></a></li>
                                            <?php endforeach; ?>
                                        </ul>
                                        <div class="clearfix"></div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </article>
                    </div>
                <?php $first_post = false; ?>
            <?php endwhile; ?>
        </div>
    <?php endif; ?>
    <?php wp_reset_query(); ?>

    <div class="row">
        <div class="col-xs-12">
                <?php echo do_shortcode('[ajax_load_more post_type="post" category="avisos" offset="7" posts_per_page="4" pause="true" scroll="false" transition="fade" button_label="Carregar mais avisos"]'); ?>
        </div>
    </div>
</section>

<?php get_footer(); ?>
