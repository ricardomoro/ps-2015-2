<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Título -->
    <title>
        <?php wp_title(); ?>
    </title>
    <link rel="alternate" type="application/rss+xml" title="<?= get_bloginfo('name'); ?> Feed" href="<?= esc_url(get_feed_link()); ?>">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo get_template_directory_uri(); ?>/img/favicon.ico"/>
    <!-- CSS & JS -->
    <?php wp_head(); ?>
</head>

<body>
    <?php echo get_template_part('partials/barrabrasil'); ?>
    <!-- Cabeçalho -->
    <header class="container">
        <h1 class="sr-only"><?php bloginfo('name'); ?></h1>
        <div class="row">
            <div class="col-xs-3 col-sm-2 col-md-2">
                <a href="<?php bloginfo('url'); ?>" title="Processo Seletivo 2015/2"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo-ifrs.png" alt="Logo do IFRS" class="center-block img-responsive"/></a>
            </div>
            <div class="col-xs-9 col-sm-6 col-md-4">
                <a href="<?php bloginfo('url'); ?>" title="Processo Seletivo 2015/2"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/topo-1.png" alt="Processo Seletivo 2015/2" class="img-responsive"/></a>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-3 hidden-xs">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/topo-2.png" alt="IFRS - Ensino Público, Gratuito e de Qualidade" class="img-responsive"/>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-3 hidden-xs hidden-sm">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/topo-3.png" alt="Processo Seletivo 2015/2 - Mais de 1100 vagas em cursos técnicos e superiores" class="img-responsive"/>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <hr id="header-separator"/>
            </div>
        </div>
    </header>
    <div class="container menu">
        <div class="row">
            <div class="col-xs-12">
                <?php echo get_template_part('partials/menu'); ?>
            </div>
        </div>
    </div>

    <a href="#inicio-conteudo" id="inicio-conteudo" class="sr-only">In&iacute;cio do conte&uacute;do</a>
